from django import forms
from models import IpSource


class IpSourceForm(forms.Form):
    class Meta:
        model = IpSource
        exclude = ['env']
