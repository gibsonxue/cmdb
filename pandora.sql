-- MySQL dump 10.14  Distrib 5.5.44-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: pandora
-- ------------------------------------------------------
-- Server version	5.5.44-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `UserManage_department`
--

DROP TABLE IF EXISTS `UserManage_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserManage_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserManage_department`
--

LOCK TABLES `UserManage_department` WRITE;
/*!40000 ALTER TABLE `UserManage_department` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserManage_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserManage_permissionlist`
--

DROP TABLE IF EXISTS `UserManage_permissionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserManage_permissionlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserManage_permissionlist`
--

LOCK TABLES `UserManage_permissionlist` WRITE;
/*!40000 ALTER TABLE `UserManage_permissionlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserManage_permissionlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserManage_rolelist`
--

DROP TABLE IF EXISTS `UserManage_rolelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserManage_rolelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserManage_rolelist`
--

LOCK TABLES `UserManage_rolelist` WRITE;
/*!40000 ALTER TABLE `UserManage_rolelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserManage_rolelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserManage_rolelist_permission`
--

DROP TABLE IF EXISTS `UserManage_rolelist_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserManage_rolelist_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rolelist_id` int(11) NOT NULL,
  `permissionlist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UserManage_rolelist_perm_rolelist_id_permissionli_f451db2a_uniq` (`rolelist_id`,`permissionlist_id`),
  KEY `UserManage_rolelist__permissionlist_id_19f919e2_fk_UserManag` (`permissionlist_id`),
  CONSTRAINT `UserManage_rolelist__permissionlist_id_19f919e2_fk_UserManag` FOREIGN KEY (`permissionlist_id`) REFERENCES `UserManage_permissionlist` (`id`),
  CONSTRAINT `UserManage_rolelist__rolelist_id_a13ecb08_fk_UserManag` FOREIGN KEY (`rolelist_id`) REFERENCES `UserManage_rolelist` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserManage_rolelist_permission`
--

LOCK TABLES `UserManage_rolelist_permission` WRITE;
/*!40000 ALTER TABLE `UserManage_rolelist_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserManage_rolelist_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserManage_user`
--

DROP TABLE IF EXISTS `UserManage_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserManage_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `nickname` varchar(64) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `UserManage_user_role_id_92ebada9_fk_UserManage_rolelist_id` (`role_id`),
  CONSTRAINT `UserManage_user_role_id_92ebada9_fk_UserManage_rolelist_id` FOREIGN KEY (`role_id`) REFERENCES `UserManage_rolelist` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserManage_user`
--

LOCK TABLES `UserManage_user` WRITE;
/*!40000 ALTER TABLE `UserManage_user` DISABLE KEYS */;
INSERT INTO `UserManage_user` VALUES (1,'pbkdf2_sha256$20000$d8t4f4q2x7rv$syo4SbcEdGp282UNHpQ4evh2LaiVHqXRWx4OaHFknPg=','2017-08-15 05:10:26','admin','admin@c.com',1,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `UserManage_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add group',1,'add_group'),(2,'Can change group',1,'change_group'),(3,'Can delete group',1,'delete_group'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add content type',3,'add_contenttype'),(8,'Can change content type',3,'change_contenttype'),(9,'Can delete content type',3,'delete_contenttype'),(10,'Can add session',4,'add_session'),(11,'Can change session',4,'change_session'),(12,'Can delete session',4,'delete_session'),(13,'Can add log entry',5,'add_logentry'),(14,'Can change log entry',5,'change_logentry'),(15,'Can delete log entry',5,'delete_logentry'),(16,'Can add permission list',6,'add_permissionlist'),(17,'Can change permission list',6,'change_permissionlist'),(18,'Can delete permission list',6,'delete_permissionlist'),(19,'Can add department',7,'add_department'),(20,'Can change department',7,'change_department'),(21,'Can delete department',7,'delete_department'),(22,'Can add role list',8,'add_rolelist'),(23,'Can change role list',8,'change_rolelist'),(24,'Can delete role list',8,'delete_rolelist'),(25,'Can add user',9,'add_user'),(26,'Can change user',9,'change_user'),(27,'Can delete user',9,'delete_user'),(28,'Can add 服务器配置',10,'add_host'),(29,'Can change 服务器配置',10,'change_host'),(30,'Can delete 服务器配置',10,'delete_host'),(31,'Can add 设备组配置',11,'add_hostgroup'),(32,'Can change 设备组配置',11,'change_hostgroup'),(33,'Can delete 设备组配置',11,'delete_hostgroup'),(34,'Can add 数据中心配置',12,'add_idc'),(35,'Can change 数据中心配置',12,'change_idc'),(36,'Can delete 数据中心配置',12,'delete_idc'),(37,'Can add env',13,'add_env'),(38,'Can change env',13,'change_env'),(39,'Can delete env',13,'delete_env'),(40,'Can add 线路配置',14,'add_interface'),(41,'Can change 线路配置',14,'change_interface'),(42,'Can delete 线路配置',14,'delete_interface'),(43,'Can add manufactory',15,'add_manufactory'),(44,'Can change manufactory',15,'change_manufactory'),(45,'Can delete manufactory',15,'delete_manufactory'),(46,'Can add 环境配置',16,'add_ipsource'),(47,'Can change 环境配置',16,'change_ipsource'),(48,'Can delete 环境配置',16,'delete_ipsource'),(49,'Can add dashboard_queue',17,'add_dashboard_queue'),(50,'Can change dashboard_queue',17,'change_dashboard_queue'),(51,'Can delete dashboard_queue',17,'delete_dashboard_queue'),(52,'Can add dashboard_status',18,'add_dashboard_status'),(53,'Can change dashboard_status',18,'change_dashboard_status'),(54,'Can delete dashboard_status',18,'delete_dashboard_status'),(55,'Can add minions_status',19,'add_minions_status'),(56,'Can change minions_status',19,'change_minions_status'),(57,'Can delete minions_status',19,'delete_minions_status'),(58,'Can add salt_returns',20,'add_salt_returns'),(59,'Can change salt_returns',20,'change_salt_returns'),(60,'Can delete salt_returns',20,'delete_salt_returns'),(61,'Can add salt_grains',21,'add_salt_grains'),(62,'Can change salt_grains',21,'change_salt_grains'),(63,'Can delete salt_grains',21,'delete_salt_grains'),(64,'Can add salt_events',22,'add_salt_events'),(65,'Can change salt_events',22,'change_salt_events'),(66,'Can delete salt_events',22,'delete_salt_events'),(67,'Can add jids',23,'add_jids'),(68,'Can change jids',23,'change_jids'),(69,'Can delete jids',23,'delete_jids'),(70,'Can add task state',24,'add_taskmeta'),(71,'Can change task state',24,'change_taskmeta'),(72,'Can delete task state',24,'delete_taskmeta'),(73,'Can add crontab',25,'add_crontabschedule'),(74,'Can change crontab',25,'change_crontabschedule'),(75,'Can delete crontab',25,'delete_crontabschedule'),(76,'Can add saved group result',26,'add_tasksetmeta'),(77,'Can change saved group result',26,'change_tasksetmeta'),(78,'Can delete saved group result',26,'delete_tasksetmeta'),(79,'Can add task',27,'add_taskstate'),(80,'Can change task',27,'change_taskstate'),(81,'Can delete task',27,'delete_taskstate'),(82,'Can add interval',28,'add_intervalschedule'),(83,'Can change interval',28,'change_intervalschedule'),(84,'Can delete interval',28,'delete_intervalschedule'),(85,'Can add periodic task',29,'add_periodictask'),(86,'Can change periodic task',29,'change_periodictask'),(87,'Can delete periodic task',29,'delete_periodictask'),(88,'Can add worker',30,'add_workerstate'),(89,'Can change worker',30,'change_workerstate'),(90,'Can delete worker',30,'delete_workerstate'),(91,'Can add periodic tasks',31,'add_periodictasks'),(92,'Can change periodic tasks',31,'change_periodictasks'),(93,'Can delete periodic tasks',31,'delete_periodictasks'),(94,'Can add 项目配置',32,'add_project'),(95,'Can change 项目配置',32,'change_project'),(96,'Can delete 项目配置',32,'delete_project');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_taskmeta`
--

DROP TABLE IF EXISTS `celery_taskmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_taskmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  `result` longtext,
  `date_done` datetime NOT NULL,
  `traceback` longtext,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `celery_taskmeta_hidden_23fd02dc` (`hidden`)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_taskmeta`
--

LOCK TABLES `celery_taskmeta` WRITE;
/*!40000 ALTER TABLE `celery_taskmeta` DISABLE KEYS */;
INSERT INTO `celery_taskmeta` VALUES (1,'8f37abc3-3749-4aae-b6c3-0d061e295439','SUCCESS',NULL,'2017-08-13 04:16:55',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(2,'f3fcd16b-6cde-4769-b355-44c5184b4816','SUCCESS',NULL,'2017-08-13 04:16:56',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(3,'95a3dda3-6317-405f-a750-ae05d72d4861','SUCCESS',NULL,'2017-08-13 04:19:55',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(4,'c8dfbe5e-e071-494e-95ac-e77d6a3a63fc','FAILURE','gAJjZGphbmdvLmRiLnV0aWxzCkludGVncml0eUVycm9yCnEBTRgEVRpDb2x1bW4gJ2lkJyBjYW5ub3QgYmUgbnVsbHEChnEDUnEELg==','2017-08-13 04:19:55','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 170, in host_sync_create\n    host.save()\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 734, in save\n    force_update=force_update, update_fields=update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 762, in save_base\n    updated = self._save_table(raw, cls, force_insert, force_update, using, update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 846, in _save_table\n    result = self._do_insert(cls._base_manager, using, fields, update_pk, raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 885, in _do_insert\n    using=using, raw=raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/manager.py\", line 127, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/query.py\", line 920, in _insert\n    return query.get_compiler(using=using).execute_sql(return_id)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/sql/compiler.py\", line 974, in execute_sql\n    cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 79, in execute\n    return super(CursorDebugWrapper, self).execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 129, in execute\n    six.reraise(utils.IntegrityError, utils.IntegrityError(*tuple(e.args)), sys.exc_info()[2])\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 124, in execute\n    return self.cursor.execute(query, args)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/cursors.py\", line 205, in execute\n    self.errorhandler(self, exc, value)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/connections.py\", line 36, in defaulterrorhandler\n    raise errorclass, errorvalue\nIntegrityError: (1048, \"Column \'id\' cannot be null\")\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(5,'0c49f502-08c4-48bd-8039-35c0d6622fbb','SUCCESS',NULL,'2017-08-13 04:19:57',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(6,'f3481eed-a5d4-4f37-b41a-74dc8aefea03','FAILURE','gAJjZGphbmdvLmRiLnV0aWxzCkludGVncml0eUVycm9yCnEBTRgEVRpDb2x1bW4gJ2lkJyBjYW5ub3QgYmUgbnVsbHEChnEDUnEELg==','2017-08-13 04:19:57','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 170, in host_sync_create\n    host.save()\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 734, in save\n    force_update=force_update, update_fields=update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 762, in save_base\n    updated = self._save_table(raw, cls, force_insert, force_update, using, update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 846, in _save_table\n    result = self._do_insert(cls._base_manager, using, fields, update_pk, raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 885, in _do_insert\n    using=using, raw=raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/manager.py\", line 127, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/query.py\", line 920, in _insert\n    return query.get_compiler(using=using).execute_sql(return_id)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/sql/compiler.py\", line 974, in execute_sql\n    cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 79, in execute\n    return super(CursorDebugWrapper, self).execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 129, in execute\n    six.reraise(utils.IntegrityError, utils.IntegrityError(*tuple(e.args)), sys.exc_info()[2])\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 124, in execute\n    return self.cursor.execute(query, args)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/cursors.py\", line 205, in execute\n    self.errorhandler(self, exc, value)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/connections.py\", line 36, in defaulterrorhandler\n    raise errorclass, errorvalue\nIntegrityError: (1048, \"Column \'id\' cannot be null\")\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(7,'bf2e3734-5873-4743-a3db-ed6e612d7bb1','SUCCESS',NULL,'2017-08-13 04:50:44',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(8,'e3ef5327-ba17-4701-a74c-d28a2b2680f9','FAILURE','gAJjZGphbmdvLmRiLnV0aWxzCk9wZXJhdGlvbmFsRXJyb3IKcQFNVAVVMkZpZWxkICd0ZXJtaW5hbF90aW1lJyBkb2Vzbid0IGhhdmUgYSBkZWZhdWx0IHZhbHVlcQKGcQNScQR9cQVVCV9fY2F1c2VfX3EGY19teXNxbF9leGNlcHRpb25zCk9wZXJhdGlvbmFsRXJyb3IKcQdNVAVoAoZxCFJxCXNiLg==','2017-08-13 04:50:44','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 170, in host_sync_create\n    host.group_id = 1\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 734, in save\n    force_update=force_update, update_fields=update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 762, in save_base\n    updated = self._save_table(raw, cls, force_insert, force_update, using, update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 846, in _save_table\n    result = self._do_insert(cls._base_manager, using, fields, update_pk, raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 885, in _do_insert\n    using=using, raw=raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/manager.py\", line 127, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/query.py\", line 920, in _insert\n    return query.get_compiler(using=using).execute_sql(return_id)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/sql/compiler.py\", line 974, in execute_sql\n    cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 79, in execute\n    return super(CursorDebugWrapper, self).execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/utils.py\", line 97, in __exit__\n    six.reraise(dj_exc_type, dj_exc_value, traceback)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 124, in execute\n    return self.cursor.execute(query, args)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/cursors.py\", line 205, in execute\n    self.errorhandler(self, exc, value)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/connections.py\", line 36, in defaulterrorhandler\n    raise errorclass, errorvalue\nOperationalError: (1364, \"Field \'terminal_time\' doesn\'t have a default value\")\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(9,'aafd9e5c-76f5-4b16-ba13-80e8a677d582','SUCCESS',NULL,'2017-08-13 04:56:26',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(10,'6838b802-2d8f-48e8-86ca-a140362fafc2','FAILURE','gAJjZGphbmdvLmRiLnV0aWxzCk9wZXJhdGlvbmFsRXJyb3IKcQFNVAVVMkZpZWxkICd0ZXJtaW5hbF90aW1lJyBkb2Vzbid0IGhhdmUgYSBkZWZhdWx0IHZhbHVlcQKGcQNScQR9cQVVCV9fY2F1c2VfX3EGY19teXNxbF9leGNlcHRpb25zCk9wZXJhdGlvbmFsRXJyb3IKcQdNVAVoAoZxCFJxCXNiLg==','2017-08-13 04:56:26','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 170, in host_sync_create\n    host.group_id = 1\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 734, in save\n    force_update=force_update, update_fields=update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 762, in save_base\n    updated = self._save_table(raw, cls, force_insert, force_update, using, update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 846, in _save_table\n    result = self._do_insert(cls._base_manager, using, fields, update_pk, raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 885, in _do_insert\n    using=using, raw=raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/manager.py\", line 127, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/query.py\", line 920, in _insert\n    return query.get_compiler(using=using).execute_sql(return_id)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/sql/compiler.py\", line 974, in execute_sql\n    cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 79, in execute\n    return super(CursorDebugWrapper, self).execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/utils.py\", line 97, in __exit__\n    six.reraise(dj_exc_type, dj_exc_value, traceback)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 124, in execute\n    return self.cursor.execute(query, args)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/cursors.py\", line 205, in execute\n    self.errorhandler(self, exc, value)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/connections.py\", line 36, in defaulterrorhandler\n    raise errorclass, errorvalue\nOperationalError: (1364, \"Field \'terminal_time\' doesn\'t have a default value\")\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(11,'17f0c872-4d7e-4c82-ab9b-5b289a2e0c01','SUCCESS',NULL,'2017-08-13 04:56:30',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(12,'18bd04f2-4f3c-4d57-b461-208b08f79bab','FAILURE','gAJjZGphbmdvLmRiLnV0aWxzCk9wZXJhdGlvbmFsRXJyb3IKcQFNVAVVMkZpZWxkICd0ZXJtaW5hbF90aW1lJyBkb2Vzbid0IGhhdmUgYSBkZWZhdWx0IHZhbHVlcQKGcQNScQR9cQVVCV9fY2F1c2VfX3EGY19teXNxbF9leGNlcHRpb25zCk9wZXJhdGlvbmFsRXJyb3IKcQdNVAVoAoZxCFJxCXNiLg==','2017-08-13 04:56:30','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 170, in host_sync_create\n    host.group_id = 1\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 734, in save\n    force_update=force_update, update_fields=update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 762, in save_base\n    updated = self._save_table(raw, cls, force_insert, force_update, using, update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 846, in _save_table\n    result = self._do_insert(cls._base_manager, using, fields, update_pk, raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 885, in _do_insert\n    using=using, raw=raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/manager.py\", line 127, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/query.py\", line 920, in _insert\n    return query.get_compiler(using=using).execute_sql(return_id)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/sql/compiler.py\", line 974, in execute_sql\n    cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 79, in execute\n    return super(CursorDebugWrapper, self).execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/utils.py\", line 97, in __exit__\n    six.reraise(dj_exc_type, dj_exc_value, traceback)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 124, in execute\n    return self.cursor.execute(query, args)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/cursors.py\", line 205, in execute\n    self.errorhandler(self, exc, value)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/connections.py\", line 36, in defaulterrorhandler\n    raise errorclass, errorvalue\nOperationalError: (1364, \"Field \'terminal_time\' doesn\'t have a default value\")\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(13,'09309862-13e6-49da-8723-d253813aeb83','SUCCESS',NULL,'2017-08-13 04:56:46',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(14,'64d3899a-dff1-4d4a-86b8-95b71b4cf5ff','FAILURE','gAJjZGphbmdvLmRiLnV0aWxzCk9wZXJhdGlvbmFsRXJyb3IKcQFNVAVVMkZpZWxkICd0ZXJtaW5hbF90aW1lJyBkb2Vzbid0IGhhdmUgYSBkZWZhdWx0IHZhbHVlcQKGcQNScQR9cQVVCV9fY2F1c2VfX3EGY19teXNxbF9leGNlcHRpb25zCk9wZXJhdGlvbmFsRXJyb3IKcQdNVAVoAoZxCFJxCXNiLg==','2017-08-13 04:56:47','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 170, in host_sync_create\n    host.group_id = 1\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 734, in save\n    force_update=force_update, update_fields=update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 762, in save_base\n    updated = self._save_table(raw, cls, force_insert, force_update, using, update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 846, in _save_table\n    result = self._do_insert(cls._base_manager, using, fields, update_pk, raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 885, in _do_insert\n    using=using, raw=raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/manager.py\", line 127, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/query.py\", line 920, in _insert\n    return query.get_compiler(using=using).execute_sql(return_id)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/sql/compiler.py\", line 974, in execute_sql\n    cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 79, in execute\n    return super(CursorDebugWrapper, self).execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/utils.py\", line 97, in __exit__\n    six.reraise(dj_exc_type, dj_exc_value, traceback)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 124, in execute\n    return self.cursor.execute(query, args)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/cursors.py\", line 205, in execute\n    self.errorhandler(self, exc, value)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/connections.py\", line 36, in defaulterrorhandler\n    raise errorclass, errorvalue\nOperationalError: (1364, \"Field \'terminal_time\' doesn\'t have a default value\")\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(15,'28843f51-104b-4022-9fed-2171c67711bd','SUCCESS',NULL,'2017-08-13 04:56:58',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(16,'a1b7ceb2-c8d4-42e5-894b-7073efb86ba2','FAILURE','gAJjZGphbmdvLmRiLnV0aWxzCk9wZXJhdGlvbmFsRXJyb3IKcQFNVAVVMkZpZWxkICd0ZXJtaW5hbF90aW1lJyBkb2Vzbid0IGhhdmUgYSBkZWZhdWx0IHZhbHVlcQKGcQNScQR9cQVVCV9fY2F1c2VfX3EGY19teXNxbF9leGNlcHRpb25zCk9wZXJhdGlvbmFsRXJyb3IKcQdNVAVoAoZxCFJxCXNiLg==','2017-08-13 04:56:58','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 170, in host_sync_create\n    host.group_id = 1\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 734, in save\n    force_update=force_update, update_fields=update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 762, in save_base\n    updated = self._save_table(raw, cls, force_insert, force_update, using, update_fields)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 846, in _save_table\n    result = self._do_insert(cls._base_manager, using, fields, update_pk, raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/base.py\", line 885, in _do_insert\n    using=using, raw=raw)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/manager.py\", line 127, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/query.py\", line 920, in _insert\n    return query.get_compiler(using=using).execute_sql(return_id)\n  File \"/usr/lib/python2.7/site-packages/django/db/models/sql/compiler.py\", line 974, in execute_sql\n    cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 79, in execute\n    return super(CursorDebugWrapper, self).execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/utils.py\", line 97, in __exit__\n    six.reraise(dj_exc_type, dj_exc_value, traceback)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/utils.py\", line 64, in execute\n    return self.cursor.execute(sql, params)\n  File \"/usr/lib/python2.7/site-packages/django/db/backends/mysql/base.py\", line 124, in execute\n    return self.cursor.execute(query, args)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/cursors.py\", line 205, in execute\n    self.errorhandler(self, exc, value)\n  File \"/usr/lib64/python2.7/site-packages/MySQLdb/connections.py\", line 36, in defaulterrorhandler\n    raise errorclass, errorvalue\nOperationalError: (1364, \"Field \'terminal_time\' doesn\'t have a default value\")\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(17,'24a2b7f8-8373-4185-bac8-3aca9ee281fd','SUCCESS',NULL,'2017-08-13 04:58:58',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(18,'7fb0c45d-29b4-4053-b857-60279dd13e4f','SUCCESS',NULL,'2017-08-13 04:58:58',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(19,'333f3cbe-5dda-403d-b97e-7c91ee66dddb','SUCCESS',NULL,'2017-08-13 05:00:41',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(20,'d877394b-52c2-4e4e-bbdd-de1584f0615d','SUCCESS',NULL,'2017-08-13 05:00:41',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(21,'20b682cf-c3bf-47ab-b94f-39d46a1f199f','SUCCESS',NULL,'2017-08-13 05:00:45',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(22,'7dda2ff9-dff9-4dc2-b4f1-193142b79af6','SUCCESS',NULL,'2017-08-13 05:00:45',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(23,'cabd2d61-e13b-40bb-a34b-6c7e3c4ff8bf','SUCCESS',NULL,'2017-08-13 05:01:34',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(24,'d14ff395-cddc-4ada-9871-930985ab9203','SUCCESS',NULL,'2017-08-13 05:01:34',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(25,'f6fe63a7-0883-45b6-a25d-575c8b0e7ad1','SUCCESS',NULL,'2017-08-13 05:03:07',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(26,'f050aa47-a5ed-4c93-add1-8eeabf9a9071','SUCCESS',NULL,'2017-08-13 05:03:07',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(27,'aa51c9fd-a7d3-4fd9-84ce-035a53c877d2','SUCCESS',NULL,'2017-08-13 05:03:14',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(28,'72c47fda-689e-4170-8f54-d0bdb6017e79','SUCCESS',NULL,'2017-08-13 05:03:14',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(29,'0c0b31b6-cf08-4031-ab51-eea5453fae94','FAILURE','gAJjZXhjZXB0aW9ucwpVbmJvdW5kTG9jYWxFcnJvcgpxAVUxbG9jYWwgdmFyaWFibGUgJ3JldCcgcmVmZXJlbmNlZCBiZWZvcmUgYXNzaWdubWVudHEChXEDUnEELg==','2017-08-15 09:59:41','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 123, in accept_grains_task\n    grains = sapi.remote_noarg_execution(minion_id, \'grains.items\')\n  File \"/root/PycharmProjects/pandora/saltcore/salt_core.py\", line 82, in remote_noarg_execution\n    return ret\nUnboundLocalError: local variable \'ret\' referenced before assignment\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(30,'11452fb6-53d6-4469-a17b-f8f5d9d54962','SUCCESS',NULL,'2017-08-15 09:59:59',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(31,'7bf9e67d-47f6-422d-83e1-fba58d055d4d','SUCCESS',NULL,'2017-08-15 10:00:10',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(32,'0d48cba5-7709-4d9b-a66f-ca2ede6d690c','SUCCESS',NULL,'2017-08-15 10:00:26',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(33,'75f722c1-c864-46b4-909a-e6482201e435','SUCCESS',NULL,'2017-08-15 10:00:42',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(34,'12d2557e-58ea-4254-ba67-1dca2bfc60a8','SUCCESS',NULL,'2017-08-15 10:00:53',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(35,'636100b8-b630-4b52-810f-3b5b16fe5cae','SUCCESS',NULL,'2017-08-15 10:01:09',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(36,'f46950b8-46ce-40e1-a0ff-e725afa71e5a','SUCCESS',NULL,'2017-08-15 10:01:25',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(37,'87aa4ea0-c74d-40d2-9993-f9a620e53b48','SUCCESS',NULL,'2017-08-15 10:01:41',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(38,'fdaa82a0-5de4-43f4-a055-fe011f0f5532','SUCCESS',NULL,'2017-08-15 10:01:57',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(39,'c8e3cf88-b6f2-40c5-ab4c-02815be52a28','SUCCESS',NULL,'2017-08-15 10:02:13',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(40,'1a72dd94-be1c-4693-aed1-ccce82290c12','SUCCESS',NULL,'2017-08-15 10:02:29',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(41,'49800bc8-1a8f-4bb3-a808-8755ad25a4bb','SUCCESS',NULL,'2017-08-15 10:02:47',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(42,'0876cd78-dc87-4cac-afaf-28182ab04ab1','SUCCESS',NULL,'2017-08-15 10:03:03',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(43,'8199b03a-d893-4236-8726-f28c8aeb2e9d','SUCCESS',NULL,'2017-08-15 10:03:19',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(44,'8930d1b4-8089-4705-beb0-3769f661fda0','SUCCESS',NULL,'2017-08-15 10:03:35',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(45,'0363b619-889c-41b9-9c1a-6d8f23f4ed7a','SUCCESS',NULL,'2017-08-15 10:03:51',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(46,'161cdc2c-208f-4fc5-899c-e10fc84a4252','SUCCESS',NULL,'2017-08-15 10:04:07',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(47,'331c547e-0580-4c0a-aaf4-8a5caa7b5fb4','SUCCESS',NULL,'2017-08-15 10:04:17',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(48,'b88d2921-fed8-4753-adce-1509d6c5ff1d','SUCCESS',NULL,'2017-08-15 10:04:33',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(49,'1de5e20a-dde7-4d8b-85bd-55abf9b39085','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:23','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(50,'2137d745-12ba-4aab-9e9e-25a0dd601777','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:24','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(51,'3805873b-768f-4f38-b400-cceb4385b958','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:24','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(52,'966c88b8-43aa-492a-9007-1610326d385c','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:25','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(53,'d37d2bad-ce13-42eb-96a7-fbcd524649cc','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:25','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(54,'59f38324-038b-4fb3-83a6-d061a326b6f8','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:26','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(55,'0e05f800-bd55-4463-b51c-66c856358704','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:26','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(56,'604ca61b-d588-4b2b-b9c1-1cad6601f8b9','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:27','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(57,'58f812d0-8d0d-45cf-845b-a5367e835d7d','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:28','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(58,'33f5e696-d3cf-4288-b808-97a1ad8bdb0b','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:28','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(59,'ccd1fa0e-01a7-4cdb-b4b8-46a22ee12608','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:29','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(60,'b1db64ce-b3be-45ac-b8f1-f37714dd6361','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:29','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(61,'9ddf9414-4658-4dbf-8fb6-066eb329d421','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:30','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(62,'b40d624a-a2df-4479-b207-b2e5c069267d','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:30','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(63,'69be73d5-94d6-494a-85bc-5d4f4d38e8a2','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:31','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(64,'a8ed4537-8c81-4a29-bfb3-38b3026326b6','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:31','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(65,'92adb505-32b0-42f6-9a5b-575297898875','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:32','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(66,'5d767429-d384-4dee-9878-73df1881a286','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:33','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(67,'dce97dc2-f951-45e8-ace6-e47337fdab74','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:33','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(68,'0ef73383-22ea-4850-96bc-ef79be0e6274','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:34','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(69,'50bea310-4377-4392-97ca-a795d4eb4703','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:34','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(70,'5cd9df7e-c069-4dd8-8b99-ffdfce9e22e3','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:35','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(71,'51c5ad8a-e6ed-485a-9b9c-60643e51a0d8','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:35','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(72,'fd2206ec-4322-4ccd-9f13-5a5a4ade7ad6','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:36','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(73,'8f0fce3f-3792-4a16-b860-030fdf84e7a2','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:37','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(74,'37a37557-084d-4158-be4c-ccd54e68a9b9','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:37','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(75,'7e3ec0bb-07b6-4ab2-b9b0-7e0dfe0dad6a','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:38','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(76,'1de729f4-b737-4949-8e0a-34042d499aaf','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:38','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(77,'e062156f-f0f6-40c1-9594-2f02f08eb8ca','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:39','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(78,'5601ad41-d443-48ef-98ea-5b7ecb8faf1d','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:39','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(79,'fe3956bd-74bd-44ce-a509-da04df5079b8','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:40','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(80,'4b984e27-0d1d-473f-8584-8d62bddc1b0f','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:45','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(81,'849146b9-c372-4180-b0a7-9929f2d0d989','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:46','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(82,'d1bc2329-d830-42d3-b19d-e282bae38586','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:46','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(83,'ec7bc775-3acd-497a-9b9e-c517cac2da79','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:47','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(84,'9fe0dc72-382f-4fa4-88f4-b2ec333fea4f','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:47','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(85,'d58f2f0e-9e2b-4d6f-8141-f33233e02e80','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:48','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(86,'f03082dc-41a5-4fe1-8381-6b8e1e81927a','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:48','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(87,'78b9a8eb-9f54-4230-9feb-1e94f1bb8d81','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:49','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(88,'353872e9-e4b9-4061-8a65-f3d06331ec7d','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:50','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(89,'ff6d0c80-3b5a-41a7-be7b-dbaae173e8a9','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:50','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(90,'6d7723ad-0b1f-4ce4-8ebc-5b8a8a520eb9','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:51','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(91,'aac5eb85-9d9d-4971-a6fa-a5aec8e952e0','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:51','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(92,'af81e571-b94d-4439-b825-c1ddb109e8fe','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:52','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(93,'fd50f280-da9f-457b-b372-249b46dcb99f','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:52','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(94,'e87d3c51-2602-40c9-a5b0-ebe1967bb93d','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:53','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(95,'919207d2-ec96-44c0-b038-65f5df3f5930','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:53','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(96,'9b44bae6-04fa-4544-8d50-2bca94d8c36d','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:54','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(97,'40511806-b9a7-4195-a9f8-3fd4d9a2bde4','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:55','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(98,'1a44f138-9400-4d40-9d3d-d7b29f042107','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:55','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(99,'b622a66c-c320-4120-88b5-66434d5b968f','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:56','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(100,'3068afcb-8861-4c10-a4d4-8aaef4319067','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:56','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(101,'28578bc4-eabc-4791-a744-43a5bb817fa2','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:57','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(102,'9f3bf6b6-680b-4b34-9503-9b81f6e2ef10','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:57','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(103,'8482c151-4473-471a-9ae5-db068dcf15b0','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:58','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(104,'d2c8c429-9b8a-4092-bf07-1629043bca72','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:58','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(105,'ce460448-094f-4c54-94f3-dedb25c4fb41','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:39:59','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(106,'dccbe2ee-c35a-4f6d-9969-b13e74cd8a2f','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:00','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(107,'add3f99f-700a-417b-9c4c-474e7d0fe6c4','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:00','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(108,'67641646-e87f-4a1f-a993-40c1524714b4','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:10','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(109,'38688e95-e065-426b-b9b7-82ee7b77624b','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:11','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(110,'42a59b0e-4309-4068-aeee-3ad65030141b','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:11','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(111,'65d06fa9-3f48-43ae-80a6-e4e407187862','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:12','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(112,'b604ca0e-0a90-4440-af26-d30843ddbcfc','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:12','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(113,'f0f5f21a-dc53-42dc-8128-f35a01a1f088','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:13','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(114,'8683c7c5-ec89-47d0-ae43-25f400d48ff6','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:13','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(115,'b4c71df5-7d67-4ce5-b048-51ce35d55764','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:14','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(116,'f20e6698-2029-4864-9c8a-9cb32dbe1b22','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:14','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(117,'aea644ae-0b96-419d-8941-3bc9056218af','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:15','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(118,'a2698742-57c3-415c-832c-57d45c8e057c','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:16','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(119,'28d4285f-96a8-4828-bee1-bce6f3c6b273','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:16','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(120,'27e43dbc-49a1-4196-8e6d-3b8bbeaffe51','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:17','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(121,'fce26ae1-677c-45c6-aa2c-1fa4ec888bd0','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:17','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(122,'b94a650d-71fc-4ac1-a406-0393d4ea4e58','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:18','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(123,'6f2a9b72-5a25-4aa2-8e72-e1b9b03396e3','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:18','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(124,'3cea9dc2-b82d-44f0-a8bf-37d6d038f6d5','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:19','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(125,'80b8bb52-5f67-4751-b84b-f3bc74201df0','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:19','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(126,'c11c9527-91e5-40b0-9daf-f5728face94c','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:20','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(127,'9177e606-9a5f-4642-aca4-95aed8035d8f','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:21','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(128,'50436de0-f7f3-43a9-9d79-c64eadebccc1','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:21','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(129,'6cf2964f-b10c-4c09-b984-cfb6a9fddf5a','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:22','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(130,'9ac91ab8-817b-4d45-b03d-af53dd3bca2e','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:22','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(131,'0c81bda4-55fd-4bde-93ea-cf2423895511','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:23','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(132,'05e00394-08a1-4cc3-8605-7b5d32d7e9db','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:23','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(133,'a5d52de4-7a0f-4a29-8393-71ac51286a02','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:24','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(134,'4fe940bf-f29e-4905-b642-4e3254861db7','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:25','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(135,'1d26adfc-2387-49bc-bd9b-23c478aa26b8','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:25','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(136,'e29fdefd-4c47-4938-8d6c-0e30c22477fd','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:26','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(137,'8a72cfb0-89cb-4225-8fb9-c578a0b59378','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:26','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(138,'0e9c2acc-8355-4c39-959b-806994d5836a','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:27','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(139,'c6ea79cb-3fc0-4a34-8afe-dcce18803cc6','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:27','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(140,'a87cde7c-bbf5-4f43-b006-0241b6868e33','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:28','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(141,'cdd259b2-9067-467d-a35c-e0f45d63c770','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:29','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(142,'fe077194-227b-44f3-b14f-099d95c043f6','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:29','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(143,'5e6efdb8-bad8-4f49-a239-db4ee4c2ca98','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:30','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(144,'6ee1176f-e8c0-4a55-8b04-8abf7628291d','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:30','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(145,'05910121-05f6-4dd3-bc2c-4346dc173142','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:31','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(146,'340de9fd-b761-4e43-9046-f23512780319','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:31','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(147,'b76e457f-eedc-4177-a5fd-424ccf25635a','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:32','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(148,'54530768-195e-4115-9b50-9590c82f8a39','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:33','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(149,'49e82e59-4e39-4c62-a7f0-7a9f03b6f3c4','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:33','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(150,'59c5e3fd-cf83-432f-a199-88c1e47d230a','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:34','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(151,'f792972e-614b-43d6-bc3d-0a107f9214d3','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:34','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(152,'5d20ce8d-713a-4678-a85f-c8a51eb9a851','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:35','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(153,'386aa359-dabd-4554-861d-c4b68c6bd209','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:36','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(154,'2c1c813e-461e-42b6-9c98-e7ac46d9e3c2','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:36','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(155,'4c29e8a7-9ecf-4a60-a89f-a27ba3f958a2','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:37','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(156,'4d114e4a-3741-43a2-8e2e-ed52ae2dff67','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:37','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(157,'8173af00-a242-4ced-b3d7-f619a87e8495','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:38','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(158,'540a4b12-2194-429d-9a3a-a201cab95c3c','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:39','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(159,'47aff626-2832-4763-95c9-1e03ec586dee','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:39','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(160,'1f9bc6e0-57e1-490d-81ad-4a29e1acfdfd','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:40','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(161,'c5998883-94a3-41f5-ad93-0597e7f42440','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:40','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(162,'fed3d464-51fe-4e21-bd97-af62e56655fb','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:41','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(163,'f1e9009f-1d01-41cd-a23b-298ad1abd960','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:42','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(164,'63cf79fb-455c-4d1e-a53c-dfd8ec41bf7f','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:42','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(165,'8c108180-ed93-41d4-af31-b7cd5e5af0ae','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:43','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(166,'1ce14409-e21b-4018-a006-e925a614da98','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:43','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(167,'303fb6e4-6be9-43d7-8b7c-f112d12c53ca','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:44','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(168,'16a2b1ce-0ac6-478c-9f42-b9957401512c','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:44','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(169,'2d13feb7-e725-4246-be16-b1c148246d22','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:45','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(170,'4275949d-1428-4078-9a0d-db6432ef6152','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:46','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(171,'2b601697-0583-45e9-94dd-e3eb32c5be69','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:46','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(172,'1ea11b9e-f005-4d61-84f9-24bc8036b305','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:47','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(173,'ee1d8e9b-5af9-4cd4-a1f3-80dc2a3e9d3e','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:47','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(174,'e57c4d54-8d89-4306-b5f1-3327f9c2211e','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:40:48','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(175,'cd2eccbb-0efd-4af8-8d3c-3b8133e735a6','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:41:12','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(176,'ffbe0732-f433-4d07-a307-5c6172b4dc68','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:41:13','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(177,'991e7012-57ae-44a3-a0c5-9610cd642c22','SUCCESS',NULL,'2017-08-22 03:41:50',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(178,'26e4d15b-9e6b-41d1-8925-ac387cdb03be','SUCCESS',NULL,'2017-08-22 03:41:52',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(179,'0fd545c7-d4ff-4ae5-a1d8-d533c170413d','SUCCESS',NULL,'2017-08-22 03:53:26',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(180,'a5f16ea1-831f-4feb-afcf-3bab9623e8c5','SUCCESS',NULL,'2017-08-22 03:53:27',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(181,'8f7d455e-0ffd-4437-98c5-aec0dc5c1918','SUCCESS',NULL,'2017-08-22 03:54:26',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(182,'51e9fd79-7c42-41d2-8307-1f2b50de8624','SUCCESS',NULL,'2017-08-22 03:54:27',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(183,'52c0ff1a-2229-4b6a-86fd-099f552b3b7b','SUCCESS',NULL,'2017-08-22 03:54:31',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(184,'237bec4f-70ca-48e0-b894-0357611fa373','SUCCESS',NULL,'2017-08-22 03:54:32',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(185,'81a6744c-a2a4-4a61-9ace-881a07cc520a','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:54:38','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(186,'55b28474-962f-4adf-b590-a85cf3fa4a0a','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:54:41','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(187,'3083a088-48a8-4051-b0d7-749b8527baf5','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:54:42','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(188,'8d2ae34f-9259-437f-892a-5765aa3739c4','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:54:52','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 19, in dashboard_task\n    up = len(status[\'up\'])\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(189,'1679e21b-0d15-487d-a223-132f0452b791','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVH3N0cmluZyBpbmRpY2VzIG11c3QgYmUgaW50ZWdlcnNxAoVxA1JxBC4=','2017-08-22 03:54:52','Traceback (most recent call last):\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/usr/lib/python2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/root/PycharmProjects/pandora/saltcore/tasks.py\", line 84, in minions_status_task\n    for host_name in status_all[\'up\']:\nTypeError: string indices must be integers\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(190,'2fbfed35-172f-4e27-b66e-6b1cc7223806','SUCCESS',NULL,'2017-08-22 03:55:38',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(191,'a3712065-09a0-4d27-a94b-f0a6bca20b85','SUCCESS',NULL,'2017-08-22 03:55:39',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(192,'6a7b76a1-6e14-40a2-a4c0-22659c0954ac','SUCCESS',NULL,'2017-08-22 05:49:07',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(193,'15c9c74c-31b6-4b4e-8931-fedc518ce91d','SUCCESS',NULL,'2017-08-22 05:49:08',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(194,'12d5a3fd-45c3-4bcc-ad61-1d41f6775c34','SUCCESS',NULL,'2017-08-22 05:56:02',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(195,'28a24a6d-58e6-412b-ae16-40d72ca70cc4','SUCCESS',NULL,'2017-08-22 05:56:04',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(196,'c7b921bb-0f1c-4399-9f92-64cd771c5c2b','SUCCESS',NULL,'2017-08-22 06:08:49',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(197,'0c8c34c9-f104-4c9c-a653-175c12944691','SUCCESS',NULL,'2017-08-22 06:08:50',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA');
/*!40000 ALTER TABLE `celery_taskmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_tasksetmeta`
--

DROP TABLE IF EXISTS `celery_tasksetmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_tasksetmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskset_id` varchar(255) NOT NULL,
  `result` longtext NOT NULL,
  `date_done` datetime NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskset_id` (`taskset_id`),
  KEY `celery_tasksetmeta_hidden_593cfc24` (`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_tasksetmeta`
--

LOCK TABLES `celery_tasksetmeta` WRITE;
/*!40000 ALTER TABLE `celery_tasksetmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `celery_tasksetmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmdb_env`
--

DROP TABLE IF EXISTS `cmdb_env`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmdb_env` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortname` varchar(30) DEFAULT NULL,
  `fullname` varchar(30) DEFAULT NULL,
  `env_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmdb_env`
--

LOCK TABLES `cmdb_env` WRITE;
/*!40000 ALTER TABLE `cmdb_env` DISABLE KEYS */;
INSERT INTO `cmdb_env` VALUES (1,'PRD','生产',1),(2,'PRE','准生产',2),(3,'SIT','测试',3);
/*!40000 ALTER TABLE `cmdb_env` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmdb_host`
--

DROP TABLE IF EXISTS `cmdb_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmdb_host` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(50) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `ip` char(39) NOT NULL,
  `ilo_ip` varchar(100) DEFAULT NULL,
  `asset_no` varchar(50) DEFAULT NULL,
  `asset_type` varchar(30) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `kernel` varchar(100) DEFAULT NULL,
  `kernel_release` varchar(100) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `cpu_model` varchar(100) DEFAULT NULL,
  `cpu_num` int(11) DEFAULT NULL,
  `memory` varchar(30) DEFAULT NULL,
  `disk` varchar(255) DEFAULT NULL,
  `disks` varchar(255) DEFAULT NULL,
  `sn` varchar(60) NOT NULL,
  `position` varchar(100) DEFAULT NULL,
  `memo` longtext,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `env_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `idc_id` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `terminal_time` datetime DEFAULT NULL,
  `vip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `cmdb_host_env_id_7c36290f_fk_cmdb_ipsource_id` (`env_id`),
  KEY `cmdb_host_group_id_bcc2faaa_fk_cmdb_hostgroup_id` (`group_id`),
  KEY `cmdb_host_idc_id_363c74a2_fk_cmdb_idc_id` (`idc_id`),
  KEY `cmdb_host_project_id_8ee5aceb_fk_publisher_project_id` (`project_id`),
  CONSTRAINT `cmdb_host_env_id_7c36290f_fk_cmdb_ipsource_id` FOREIGN KEY (`env_id`) REFERENCES `cmdb_ipsource` (`id`),
  CONSTRAINT `cmdb_host_group_id_bcc2faaa_fk_cmdb_hostgroup_id` FOREIGN KEY (`group_id`) REFERENCES `cmdb_hostgroup` (`id`),
  CONSTRAINT `cmdb_host_idc_id_363c74a2_fk_cmdb_idc_id` FOREIGN KEY (`idc_id`) REFERENCES `cmdb_idc` (`id`),
  CONSTRAINT `cmdb_host_project_id_8ee5aceb_fk_publisher_project_id` FOREIGN KEY (`project_id`) REFERENCES `publisher_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmdb_host`
--

LOCK TABLES `cmdb_host` WRITE;
/*!40000 ALTER TABLE `cmdb_host` DISABLE KEYS */;
INSERT INTO `cmdb_host` VALUES (1,'s1-sit-django-tomcat7-01','uuid','192.168.2.5','192.168.2.1','D3E3DCA','1','1','WINDOWS','3.10.0-327.el7.x86_64','CentOS7.2.1511','VMWARE','E7-1232 v2 2.7',4,'512','512','\\dev\\sdb','D3E3DCA','e1-e2-2-3','','2017-08-13 04:43:05','2017-08-13 04:46:48',3,2,1,3,'2017-08-07 00:00:00','192.168.2.1'),(9,'s1-sit-django-tomcat9-01','UUDI-1231231202130-12312213','192.168.2.13','','','1','1','Windows','3.10.0-327.el7.x86#_64','CentOS7.2.1511','','centos9123123',3,'100','100','100','12312',NULL,NULL,'2017-08-13 12:29:10','2017-08-13 12:29:10',3,1,NULL,3,NULL,''),(10,'s1-sit-django-tomcat10-10','ladskfjlkjasd;ljl;kjl','192.168.2.31','192.168.3.21',NULL,'1','1','Windows','3.10.0-327.el7.x86#_64','CentOS7.2.1511',NULL,'123312321',2,'10','1000','20','123912031209321',NULL,NULL,'2017-08-13 12:53:04','2017-08-13 12:53:04',3,1,NULL,1,NULL,'192.168.3.1'),(11,'s1-prd-django-tomcat7-01','jklajsdlkjfadklj','192.168.3.21','192.168.3.12',NULL,'1','1','Windows','3.10.0-327.el7.x86#_64','CentOS7.2.1511',NULL,'CENTOS12.2',2,'100','100','1010','DKESLCA',NULL,NULL,'2017-08-13 12:56:44','2017-08-13 12:56:44',4,1,NULL,1,NULL,'192.168.2.13'),(12,'s1-disa-dkdk','12lkjlkadjfkljlk','192.168.2.2','192.168.3.12',NULL,'2','1','Windows','3.10.0-327.el7.x86#_64','CentOS7.2.1511',NULL,'dsakljafdl',2,'100','1002','192','aoijadlk;j',NULL,NULL,'2017-08-13 12:57:45','2017-08-13 12:57:45',3,1,NULL,1,NULL,'192.168.3.12'),(13,'s1-prd-django-tomcat0-10','alkjlkjasfd','192.168.3.21','192.168.2.13','adlkfjlk',NULL,'1','Windows','3.10.0-327.el7.x86_64','CentOS7.2.1511','VMWARE','adfs',12,'100','100','100','lklk123lkj',NULL,NULL,'2017-08-13 13:41:56','2017-08-13 13:41:56',NULL,1,NULL,1,'2017-08-15 00:00:00','192.168.3.12'),(38,'PyDev2','7f664d56-d7225-8457-7b01-e20b889ba5a7','127.0.0.1',NULL,NULL,'2','1','Linux','3.10.0-327.el7.x86_64','CentOS7.2.1511','VMware, Inc.','Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz',4,'3937',NULL,'[u\'sda\', u\'sr0\', u\'dm-0\', u\'dm-1\', u\'dm-2\']','VMware-56 4d 66 7f 25 d7 57 84-7b 01 e2 0b 88 9b a5 a7',NULL,NULL,'2017-08-22 03:42:51','2017-08-22 03:42:51',1,1,NULL,1,NULL,NULL),(40,'PyDev4','7f64d56-d725-8457-7b01-e20b889ba5a7','127.0.0.1',NULL,NULL,'2','1','Linux','3.10.0-327.el7.x86_64','CentOS7.2.1511','VMware, Inc.','Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz',4,'3937',NULL,'[u\'sda\', u\'sr0\', u\'dm-0\', u\'dm-1\', u\'dm-2\']','VMware-56 4d 66 7f 25 d7 57 84-7b 01 e2 0b 88 9b a5 a7',NULL,NULL,'2017-08-22 03:55:53','2017-08-22 03:55:53',1,1,NULL,1,NULL,NULL),(41,'PyDev3','7f664d56-d7s5-8457-7b01-e20b889ba5a7','127.0.0.1',NULL,NULL,'2','1','Linux','3.10.0-327.el7.x86_64','CentOS7.2.1511','VMware, Inc.','Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz',4,'3937',NULL,'[u\'sda\', u\'sr0\', u\'dm-0\', u\'dm-1\', u\'dm-2\']','VMware-56 4d 66 7f 25 d7 57 84-7b 01 e2 0b 88 9b a5 a7',NULL,NULL,'2017-08-22 03:55:53','2017-08-22 03:55:53',1,1,NULL,1,NULL,NULL),(42,'PyDev','7f664d56-d725-8457-7b01-e20b889ba5a7','127.0.0.1','','','2','1','Linux','3.10.0-327.el7.x86_64','CentOS7.2.1511','VMware, Inc.','Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz',4,'3937','','[u\'sda\', u\'sr0\', u\'dm-0\', u\'dm-1\', u\'dm-2\']','VMware-56 4d 66 7f 25 d7 57 84-7b 01 e2 0b 88 9b a5 a7',NULL,NULL,'2017-08-22 03:55:53','2017-08-22 03:55:53',1,1,NULL,3,'2017-08-08 00:00:00','');
/*!40000 ALTER TABLE `cmdb_host` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmdb_hostgroup`
--

DROP TABLE IF EXISTS `cmdb_hostgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmdb_hostgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmdb_hostgroup`
--

LOCK TABLES `cmdb_hostgroup` WRITE;
/*!40000 ALTER TABLE `cmdb_hostgroup` DISABLE KEYS */;
INSERT INTO `cmdb_hostgroup` VALUES (1,'Django',NULL),(2,'PHP-ADMIN',NULL);
/*!40000 ALTER TABLE `cmdb_hostgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmdb_idc`
--

DROP TABLE IF EXISTS `cmdb_idc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmdb_idc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `contact` varchar(30) DEFAULT NULL,
  `contact_phone` varchar(30) DEFAULT NULL,
  `jigui` varchar(30) DEFAULT NULL,
  `ip_range` varchar(30) DEFAULT NULL,
  `bandwidth` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmdb_idc`
--

LOCK TABLES `cmdb_idc` WRITE;
/*!40000 ALTER TABLE `cmdb_idc` DISABLE KEYS */;
INSERT INTO `cmdb_idc` VALUES (1,'SHANGHAI','-','-','-','-','-','-','-');
/*!40000 ALTER TABLE `cmdb_idc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmdb_interface`
--

DROP TABLE IF EXISTS `cmdb_interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmdb_interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `vendor` varchar(30) DEFAULT NULL,
  `bandwidth` varchar(30) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `contact` varchar(30) DEFAULT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmdb_interface`
--

LOCK TABLES `cmdb_interface` WRITE;
/*!40000 ALTER TABLE `cmdb_interface` DISABLE KEYS */;
/*!40000 ALTER TABLE `cmdb_interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmdb_ipsource`
--

DROP TABLE IF EXISTS `cmdb_ipsource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmdb_ipsource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vlan_id` int(11) NOT NULL,
  `subnet` varchar(30) DEFAULT NULL,
  `describe` varchar(30) DEFAULT NULL,
  `env_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cmdb_ipsource_env_id_27062449_fk` (`env_id`),
  CONSTRAINT `cmdb_ipsource_env_id_27062449_fk` FOREIGN KEY (`env_id`) REFERENCES `cmdb_env` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmdb_ipsource`
--

LOCK TABLES `cmdb_ipsource` WRITE;
/*!40000 ALTER TABLE `cmdb_ipsource` DISABLE KEYS */;
INSERT INTO `cmdb_ipsource` VALUES (1,0,'0.0.0.0','VLAN0',1),(3,1,'192.168.2.0/24','VLAN1',1),(4,2,'192.168.3.0/24','VLAN3',1);
/*!40000 ALTER TABLE `cmdb_ipsource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cmdb_manufactory`
--

DROP TABLE IF EXISTS `cmdb_manufactory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cmdb_manufactory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(30) NOT NULL,
  `asset_type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cmdb_manufactory`
--

LOCK TABLES `cmdb_manufactory` WRITE;
/*!40000 ALTER TABLE `cmdb_manufactory` DISABLE KEYS */;
INSERT INTO `cmdb_manufactory` VALUES (1,'VMWARE','1'),(2,'VM','1');
/*!40000 ALTER TABLE `cmdb_manufactory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_queue`
--

DROP TABLE IF EXISTS `dashboard_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` varchar(32) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_queue`
--

LOCK TABLES `dashboard_queue` WRITE;
/*!40000 ALTER TABLE `dashboard_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_status`
--

DROP TABLE IF EXISTS `dashboard_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `up` int(11) DEFAULT NULL,
  `down` int(11) DEFAULT NULL,
  `accepted` int(11) DEFAULT NULL,
  `unaccepted` int(11) DEFAULT NULL,
  `rejected` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_status`
--

LOCK TABLES `dashboard_status` WRITE;
/*!40000 ALTER TABLE `dashboard_status` DISABLE KEYS */;
INSERT INTO `dashboard_status` VALUES (1,1,0,1,0,0);
/*!40000 ALTER TABLE `dashboard_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_UserManage_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_UserManage_user_id` FOREIGN KEY (`user_id`) REFERENCES `UserManage_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2017-08-13 04:25:55','1','生产',1,'[{\"added\": {}}]',13,1),(2,'2017-08-13 04:26:15','2','准生产',1,'[{\"added\": {}}]',13,1),(3,'2017-08-13 04:26:35','3','测试',1,'[{\"added\": {}}]',13,1),(4,'2017-08-13 04:41:49','3','192.168.2.0/24',1,'[{\"added\": {}}]',16,1),(5,'2017-08-13 04:45:23','1','Django',1,'[{\"added\": {}}]',11,1),(6,'2017-08-13 04:45:40','1','SHANGHAI',1,'[{\"added\": {}}]',12,1),(7,'2017-08-13 04:46:02','1','/',1,'[{\"added\": {}}]',32,1),(8,'2017-08-13 04:46:33','2','Django',1,'[{\"added\": {}}]',32,1),(9,'2017-08-13 04:46:48','1','s1-sit-django-tomcat7-01',1,'[{\"added\": {}}]',10,1),(10,'2017-08-13 06:12:26','4','192.168.3.0/24',1,'[{\"added\": {}}]',16,1),(11,'2017-08-13 06:36:47','1','Manufactory object',1,'[{\"added\": {}}]',15,1),(12,'2017-08-13 06:36:54','2','Manufactory object',1,'[{\"added\": {}}]',15,1),(13,'2017-08-13 11:40:15','2','Manufactory object',2,'[{\"changed\": {\"fields\": [\"asset_type\"]}}]',15,1),(14,'2017-08-13 11:40:22','1','Manufactory object',2,'[{\"changed\": {\"fields\": [\"asset_type\"]}}]',15,1),(15,'2017-08-15 05:52:51','8','PyDev',2,'Changed project.',10,1),(16,'2017-08-15 05:53:59','3','CANGO-ERP-01',1,'',32,1),(17,'2017-08-15 05:54:02','8','PyDev',2,'Changed project.',10,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (5,'admin','logentry'),(1,'auth','group'),(2,'auth','permission'),(13,'cmdb','env'),(10,'cmdb','host'),(11,'cmdb','hostgroup'),(12,'cmdb','idc'),(14,'cmdb','interface'),(16,'cmdb','ipsource'),(15,'cmdb','manufactory'),(3,'contenttypes','contenttype'),(17,'dashboard','dashboard_queue'),(18,'dashboard','dashboard_status'),(25,'djcelery','crontabschedule'),(28,'djcelery','intervalschedule'),(29,'djcelery','periodictask'),(31,'djcelery','periodictasks'),(24,'djcelery','taskmeta'),(26,'djcelery','tasksetmeta'),(27,'djcelery','taskstate'),(30,'djcelery','workerstate'),(19,'minions','minions_status'),(32,'publisher','project'),(23,'returner','jids'),(22,'returner','salt_events'),(21,'returner','salt_grains'),(20,'returner','salt_returns'),(4,'sessions','session'),(7,'UserManage','department'),(6,'UserManage','permissionlist'),(8,'UserManage','rolelist'),(9,'UserManage','user');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'UserManage','0001_initial','2017-08-13 04:02:51'),(2,'contenttypes','0001_initial','2017-08-13 04:02:51'),(3,'admin','0001_initial','2017-08-13 04:02:51'),(4,'admin','0002_logentry_remove_auto_add','2017-08-13 04:02:51'),(5,'contenttypes','0002_remove_content_type_name','2017-08-13 04:02:51'),(6,'auth','0001_initial','2017-08-13 04:02:52'),(7,'auth','0002_alter_permission_name_max_length','2017-08-13 04:02:52'),(8,'auth','0003_alter_user_email_max_length','2017-08-13 04:02:52'),(9,'auth','0004_alter_user_username_opts','2017-08-13 04:02:52'),(10,'auth','0005_alter_user_last_login_null','2017-08-13 04:02:52'),(11,'auth','0006_require_contenttypes_0002','2017-08-13 04:02:52'),(12,'auth','0007_alter_validators_add_error_messages','2017-08-13 04:02:52'),(13,'auth','0008_alter_user_username_max_length','2017-08-13 04:02:52'),(14,'publisher','0001_initial','2017-08-13 04:02:52'),(15,'cmdb','0001_initial','2017-08-13 04:02:52'),(16,'dashboard','0001_initial','2017-08-13 04:02:52'),(17,'djcelery','0001_initial','2017-08-13 04:02:52'),(18,'minions','0001_initial','2017-08-13 04:02:52'),(19,'returner','0001_initial','2017-08-13 04:02:52'),(20,'sessions','0001_initial','2017-08-13 04:02:52'),(21,'cmdb','0002_host_terminal_time','2017-08-13 04:39:53'),(22,'cmdb','0003_auto_20170813_1239','2017-08-13 04:39:53'),(23,'cmdb','0004_auto_20170813_1258','2017-08-13 04:58:16'),(24,'cmdb','0005_auto_20170813_1310','2017-08-13 05:10:46'),(25,'cmdb','0006_host_host_id','2017-08-13 05:14:36'),(26,'cmdb','0007_remove_host_host_id','2017-08-13 05:25:00'),(27,'cmdb','0008_auto_20170813_1331','2017-08-13 05:31:56'),(28,'cmdb','0002_auto_20170813_2124','2017-08-13 13:24:59'),(29,'cmdb','0002_auto_20170811_1234','2017-08-16 08:00:25'),(30,'cmdb','0002_auto_20170822_1355','2017-08-22 05:55:17'),(31,'cmdb','0003_auto_20170822_1410','2017-08-22 06:10:06');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('58neh56bhxbr9dff06gmw8s81qpe2w4x','MzA3NzQwNTUxNzkyNmNiZDg1M2VmZjNmNTgxOGU3OTAwYThlNTY3NTp7Il9hdXRoX3VzZXJfaGFzaCI6IjI2NGFmMWViOWFlMjdhOTY2MjIzN2ZkMGQwNjljMzM2NWYxOWRmNGUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-08-27 06:31:47'),('9v7j7gvqlt2cgdhlx37vajgz680mrq1d','MzA3NzQwNTUxNzkyNmNiZDg1M2VmZjNmNTgxOGU3OTAwYThlNTY3NTp7Il9hdXRoX3VzZXJfaGFzaCI6IjI2NGFmMWViOWFlMjdhOTY2MjIzN2ZkMGQwNjljMzM2NWYxOWRmNGUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-08-27 04:05:12'),('u15djcodnwbv1a46zzkrgkpahc6yd539','OWYyOGJjMzU5YWZiNzZiMTc2NDE2YTZiY2VmMTQyMzVlYTJjOTUzMTp7Il9hdXRoX3VzZXJfaGFzaCI6IjY4MzhhODBjMjY2OGM5ZDJjNjE4NDFiOTA0N2E0YWM5Mzk2ODA4OTQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-08-29 05:10:26');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_crontabschedule`
--

DROP TABLE IF EXISTS `djcelery_crontabschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_crontabschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minute` varchar(64) NOT NULL,
  `hour` varchar(64) NOT NULL,
  `day_of_week` varchar(64) NOT NULL,
  `day_of_month` varchar(64) NOT NULL,
  `month_of_year` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_crontabschedule`
--

LOCK TABLES `djcelery_crontabschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_crontabschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_crontabschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_intervalschedule`
--

DROP TABLE IF EXISTS `djcelery_intervalschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_intervalschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `every` int(11) NOT NULL,
  `period` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_intervalschedule`
--

LOCK TABLES `djcelery_intervalschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_intervalschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_intervalschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictask`
--

DROP TABLE IF EXISTS `djcelery_periodictask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `task` varchar(200) NOT NULL,
  `args` longtext NOT NULL,
  `kwargs` longtext NOT NULL,
  `queue` varchar(200) DEFAULT NULL,
  `exchange` varchar(200) DEFAULT NULL,
  `routing_key` varchar(200) DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime DEFAULT NULL,
  `total_run_count` int(10) unsigned NOT NULL,
  `date_changed` datetime NOT NULL,
  `description` longtext NOT NULL,
  `crontab_id` int(11) DEFAULT NULL,
  `interval_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` (`crontab_id`),
  KEY `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` (`interval_id`),
  CONSTRAINT `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`),
  CONSTRAINT `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictask`
--

LOCK TABLES `djcelery_periodictask` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictask` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictasks`
--

DROP TABLE IF EXISTS `djcelery_periodictasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictasks` (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`ident`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictasks`
--

LOCK TABLES `djcelery_periodictasks` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_taskstate`
--

DROP TABLE IF EXISTS `djcelery_taskstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_taskstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(64) NOT NULL,
  `task_id` varchar(36) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `tstamp` datetime NOT NULL,
  `args` longtext,
  `kwargs` longtext,
  `eta` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `result` longtext,
  `traceback` longtext,
  `runtime` double DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `worker_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `djcelery_taskstate_state_53543be4` (`state`),
  KEY `djcelery_taskstate_name_8af9eded` (`name`),
  KEY `djcelery_taskstate_tstamp_4c3f93a1` (`tstamp`),
  KEY `djcelery_taskstate_hidden_c3905e57` (`hidden`),
  KEY `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` (`worker_id`),
  CONSTRAINT `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_taskstate`
--

LOCK TABLES `djcelery_taskstate` WRITE;
/*!40000 ALTER TABLE `djcelery_taskstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_taskstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_workerstate`
--

DROP TABLE IF EXISTS `djcelery_workerstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_workerstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `last_heartbeat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  KEY `djcelery_workerstate_last_heartbeat_4539b544` (`last_heartbeat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_workerstate`
--

LOCK TABLES `djcelery_workerstate` WRITE;
/*!40000 ALTER TABLE `djcelery_workerstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_workerstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jids`
--

DROP TABLE IF EXISTS `jids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jid` varchar(225) NOT NULL,
  `load` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jid` (`jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jids`
--

LOCK TABLES `jids` WRITE;
/*!40000 ALTER TABLE `jids` DISABLE KEYS */;
/*!40000 ALTER TABLE `jids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `minions_status`
--

DROP TABLE IF EXISTS `minions_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `minions_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minion_id` varchar(128) DEFAULT NULL,
  `minion_version` varchar(128) DEFAULT NULL,
  `minion_status` varchar(128) DEFAULT NULL,
  `minion_config` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `minions_status`
--

LOCK TABLES `minions_status` WRITE;
/*!40000 ALTER TABLE `minions_status` DISABLE KEYS */;
INSERT INTO `minions_status` VALUES (5,'PyDev','2016.11.6','Up',0);
/*!40000 ALTER TABLE `minions_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher_project`
--

DROP TABLE IF EXISTS `publisher_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publisher_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `show` tinyint(1) NOT NULL,
  `url` varchar(300) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `permission_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `publisher_project_parent_id_7bd7c8f6_fk_publisher_project_id` (`parent_id`),
  CONSTRAINT `publisher_project_parent_id_7bd7c8f6_fk_publisher_project_id` FOREIGN KEY (`parent_id`) REFERENCES `publisher_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher_project`
--

LOCK TABLES `publisher_project` WRITE;
/*!40000 ALTER TABLE `publisher_project` DISABLE KEYS */;
INSERT INTO `publisher_project` VALUES (1,'/',1,'javascript:void(0)',-1,1,NULL),(2,'Django',1,'javascript:void(0)',-1,1,NULL),(3,'CANGO-ERP-01',1,'javascript:void(0)',-1,1,2);
/*!40000 ALTER TABLE `publisher_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher_project_owner`
--

DROP TABLE IF EXISTS `publisher_project_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publisher_project_owner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `publisher_project_owner_project_id_user_id_9d187401_uniq` (`project_id`,`user_id`),
  KEY `publisher_project_owner_user_id_2e2e1cd3_fk_UserManage_user_id` (`user_id`),
  CONSTRAINT `publisher_project_owner_user_id_2e2e1cd3_fk_UserManage_user_id` FOREIGN KEY (`user_id`) REFERENCES `UserManage_user` (`id`),
  CONSTRAINT `publisher_project_ow_project_id_d7a73b22_fk_publisher` FOREIGN KEY (`project_id`) REFERENCES `publisher_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher_project_owner`
--

LOCK TABLES `publisher_project_owner` WRITE;
/*!40000 ALTER TABLE `publisher_project_owner` DISABLE KEYS */;
/*!40000 ALTER TABLE `publisher_project_owner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salt_events`
--

DROP TABLE IF EXISTS `salt_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salt_events` (
  `alter_time` datetime NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL,
  `data` longtext NOT NULL,
  `minion_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salt_events`
--

LOCK TABLES `salt_events` WRITE;
/*!40000 ALTER TABLE `salt_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `salt_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salt_grains`
--

DROP TABLE IF EXISTS `salt_grains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salt_grains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minion_id` varchar(255) DEFAULT NULL,
  `grains` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salt_grains`
--

LOCK TABLES `salt_grains` WRITE;
/*!40000 ALTER TABLE `salt_grains` DISABLE KEYS */;
INSERT INTO `salt_grains` VALUES (2,'PyDev4','{u\'biosversion\': u\'6.00\', u\'kernel\': u\'Linux\', u\'domain\': u\'localdomain\', u\'uid\': 0, u\'zmqversion\': u\'4.1.4\', u\'kernelrelease\': u\'3.10.0-327.el7.x86_64\', u\'selinux\': {u\'enforced\': u\'Enforcing\', u\'enabled\': True}, u\'serialnumber\': u\'VMware-56 4d 66 7f 25 d7 57 84-7b 01 e2 0b 88 9b a5 a7\', u\'pid\': 97964, u\'ip_interfaces\': {u\'lo\': [u\'127.0.0.1\', u\'::1\'], u\'eno16777736\': [u\'192.168.20.22\', u\'fe80::20c:29ff:fe9b:a5a7\'], u\'virbr0-nic\': [], u\'virbr0\': [u\'192.168.122.1\']}, u\'groupname\': u\'root\', u\'shell\': u\'/bin/sh\', u\'mem_total\': 3937, u\'saltversioninfo\': [2016, 11, 6, 0], u\'osmajorrelease\': u\'7\', u\'SSDs\': [], u\'mdadm\': [], u\'id\': u\'PyDev4\', u\'osrelease\': u\'7.2.1511\', u\'ps\': u\'ps -efH\', u\'systemd\': {u\'version\': u\'219\', u\'features\': u\'+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ -LZ4 -SECCOMP +BLKID +ELFUTILS +KMOD +IDN\'}, u\'uuid\': u\'7f64d56-d725-8457-7b01-e20b889ba5a7\', u\'ip6_interfaces\': {u\'lo\': [u\'::1\'], u\'eno16777736\': [u\'fe80::20c:29ff:fe9b:a5a7\'], u\'virbr0-nic\': [], u\'virbr0\': []}, u\'num_cpus\': 4, u\'hwaddr_interfaces\': {u\'lo\': u\'00:00:00:00:00:00\', u\'eno16777736\': u\'00:0c:29:9b:a5:a7\', u\'virbr0-nic\': u\'52:54:00:9b:0e:3c\', u\'virbr0\': u\'52:54:00:9b:0e:3c\'}, u\'init\': u\'systemd\', u\'ip4_interfaces\': {u\'lo\': [u\'127.0.0.1\'], u\'eno16777736\': [u\'192.168.20.22\'], u\'virbr0-nic\': [], u\'virbr0\': [u\'192.168.122.1\']}, u\'osfullname\': u\'CentOS Linux\', u\'gid\': 0, u\'master\': u\'127.0.0.1\', u\'ipv4\': [u\'127.0.0.1\', u\'192.168.20.22\', u\'192.168.122.1\'], u\'dns\': {u\'domain\': u\'\', u\'sortlist\': [], u\'nameservers\': [u\'202.96.209.5\', u\'114.114.114.114\'], u\'ip4_nameservers\': [u\'202.96.209.5\', u\'114.114.114.114\'], u\'search\': [], u\'ip6_nameservers\': [], u\'options\': []}, u\'ipv6\': [u\'::1\', u\'fe80::20c:29ff:fe9b:a5a7\'], u\'cpu_flags\': [u\'fpu\', u\'vme\', u\'de\', u\'pse\', u\'tsc\', u\'msr\', u\'pae\', u\'mce\', u\'cx8\', u\'apic\', u\'sep\', u\'mtrr\', u\'pge\', u\'mca\', u\'cmov\', u\'pat\', u\'pse36\', u\'clflush\', u\'dts\', u\'mmx\', u\'fxsr\', u\'sse\', u\'sse2\', u\'ss\', u\'syscall\', u\'nx\', u\'pdpe1gb\', u\'rdtscp\', u\'lm\', u\'constant_tsc\', u\'arch_perfmon\', u\'pebs\', u\'bts\', u\'nopl\', u\'xtopology\', u\'tsc_reliable\', u\'nonstop_tsc\', u\'aperfmperf\', u\'eagerfpu\', u\'pni\', u\'pclmulqdq\', u\'ssse3\', u\'fma\', u\'cx16\', u\'pcid\', u\'sse4_1\', u\'sse4_2\', u\'x2apic\', u\'movbe\', u\'popcnt\', u\'tsc_deadline_timer\', u\'aes\', u\'xsave\', u\'avx\', u\'f16c\', u\'rdrand\', u\'hypervisor\', u\'lahf_lm\', u\'abm\', u\'3dnowprefetch\', u\'ida\', u\'arat\', u\'epb\', u\'pln\', u\'pts\', u\'dtherm\', u\'hwp\', u\'hwp_noitfy\', u\'hwp_act_window\', u\'hwp_epp\', u\'fsgsbase\', u\'tsc_adjust\', u\'bmi1\', u\'hle\', u\'avx2\', u\'smep\', u\'bmi2\', u\'invpcid\', u\'rtm\', u\'rdseed\', u\'adx\', u\'smap\', u\'xsaveopt\'], u\'localhost\': u\'PyDev\', u\'lsb_distrib_id\': u\'CentOS Linux\', u\'username\': u\'root\', u\'fqdn_ip4\': [u\'127.0.0.1\'], u\'saltpath\': u\'/usr/lib/python2.7/site-packages/salt\', u\'fqdn_ip6\': [u\'::1\'], u\'nodename\': u\'PyDev\', u\'saltversion\': u\'2016.11.6\', u\'pythonpath\': [u\'/usr/bin\', u\'/usr/lib64/python27.zip\', u\'/usr/lib64/python2.7\', u\'/usr/lib64/python2.7/plat-linux2\', u\'/usr/lib64/python2.7/lib-tk\', u\'/usr/lib64/python2.7/lib-old\', u\'/usr/lib64/python2.7/lib-dynload\', u\'/usr/lib64/python2.7/site-packages\', u\'/usr/lib64/python2.7/site-packages/gtk-2.0\', u\'/usr/lib/python2.7/site-packages\'], u\'server_id\': 1871050395, u\'biosreleasedate\': u\'07/02/2015\', u\'host\': u\'localhost\', u\'os_family\': u\'RedHat\', u\'oscodename\': u\'CentOS Linux 7 (Core)\', u\'osfinger\': u\'CentOS Linux-7\', u\'pythonversion\': [2, 7, 5, u\'final\', 0], u\'manufacturer\': u\'VMware, Inc.\', u\'num_gpus\': 1, u\'virtual\': u\'VMware\', u\'disks\': [u\'sda\', u\'sr0\', u\'dm-0\', u\'dm-1\', u\'dm-2\'], u\'cpu_model\': u\'Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz\', u\'fqdn\': u\'localhost.localdomain\', u\'pythonexecutable\': u\'/usr/bin/python\', u\'productname\': u\'VMware Virtual Platform\', u\'osarch\': u\'x86_64\', u\'cpuarch\': u\'x86_64\', u\'lsb_distrib_codename\': u\'CentOS Linux 7 (Core)\', u\'osrelease_info\': [7, 2, 1511], u\'locale_info\': {u\'detectedencoding\': u\'UTF-8\', u\'defaultlanguage\': u\'en_US\', u\'defaultencoding\': u\'UTF-8\'}, u\'gpus\': [{u\'model\': u\'SVGA II Adapter\', u\'vendor\': u\'unknown\'}], u\'path\': u\'/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/bin:/sbin\', u\'machine_id\': u\'25770a244db14dacb44ca4f769e0724d\', u\'os\': u\'CentOS\'}'),(3,'PyDev3','{u\'biosversion\': u\'6.00\', u\'kernel\': u\'Linux\', u\'domain\': u\'localdomain\', u\'uid\': 0, u\'zmqversion\': u\'4.1.4\', u\'kernelrelease\': u\'3.10.0-327.el7.x86_64\', u\'selinux\': {u\'enforced\': u\'Enforcing\', u\'enabled\': True}, u\'serialnumber\': u\'VMware-56 4d 66 7f 25 d7 57 84-7b 01 e2 0b 88 9b a5 a7\', u\'pid\': 97964, u\'ip_interfaces\': {u\'lo\': [u\'127.0.0.1\', u\'::1\'], u\'eno16777736\': [u\'192.168.20.22\', u\'fe80::20c:29ff:fe9b:a5a7\'], u\'virbr0-nic\': [], u\'virbr0\': [u\'192.168.122.1\']}, u\'groupname\': u\'root\', u\'shell\': u\'/bin/sh\', u\'mem_total\': 3937, u\'saltversioninfo\': [2016, 11, 6, 0], u\'osmajorrelease\': u\'7\', u\'SSDs\': [], u\'mdadm\': [], u\'id\': u\'PyDev3\', u\'osrelease\': u\'7.2.1511\', u\'ps\': u\'ps -efH\', u\'systemd\': {u\'version\': u\'219\', u\'features\': u\'+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ -LZ4 -SECCOMP +BLKID +ELFUTILS +KMOD +IDN\'}, u\'uuid\': u\'7f664d56-d7s5-8457-7b01-e20b889ba5a7\', u\'ip6_interfaces\': {u\'lo\': [u\'::1\'], u\'eno16777736\': [u\'fe80::20c:29ff:fe9b:a5a7\'], u\'virbr0-nic\': [], u\'virbr0\': []}, u\'num_cpus\': 4, u\'hwaddr_interfaces\': {u\'lo\': u\'00:00:00:00:00:00\', u\'eno16777736\': u\'00:0c:29:9b:a5:a7\', u\'virbr0-nic\': u\'52:54:00:9b:0e:3c\', u\'virbr0\': u\'52:54:00:9b:0e:3c\'}, u\'init\': u\'systemd\', u\'ip4_interfaces\': {u\'lo\': [u\'127.0.0.1\'], u\'eno16777736\': [u\'192.168.20.22\'], u\'virbr0-nic\': [], u\'virbr0\': [u\'192.168.122.1\']}, u\'osfullname\': u\'CentOS Linux\', u\'gid\': 0, u\'master\': u\'127.0.0.1\', u\'ipv4\': [u\'127.0.0.1\', u\'192.168.20.22\', u\'192.168.122.1\'], u\'dns\': {u\'domain\': u\'\', u\'sortlist\': [], u\'nameservers\': [u\'202.96.209.5\', u\'114.114.114.114\'], u\'ip4_nameservers\': [u\'202.96.209.5\', u\'114.114.114.114\'], u\'search\': [], u\'ip6_nameservers\': [], u\'options\': []}, u\'ipv6\': [u\'::1\', u\'fe80::20c:29ff:fe9b:a5a7\'], u\'cpu_flags\': [u\'fpu\', u\'vme\', u\'de\', u\'pse\', u\'tsc\', u\'msr\', u\'pae\', u\'mce\', u\'cx8\', u\'apic\', u\'sep\', u\'mtrr\', u\'pge\', u\'mca\', u\'cmov\', u\'pat\', u\'pse36\', u\'clflush\', u\'dts\', u\'mmx\', u\'fxsr\', u\'sse\', u\'sse2\', u\'ss\', u\'syscall\', u\'nx\', u\'pdpe1gb\', u\'rdtscp\', u\'lm\', u\'constant_tsc\', u\'arch_perfmon\', u\'pebs\', u\'bts\', u\'nopl\', u\'xtopology\', u\'tsc_reliable\', u\'nonstop_tsc\', u\'aperfmperf\', u\'eagerfpu\', u\'pni\', u\'pclmulqdq\', u\'ssse3\', u\'fma\', u\'cx16\', u\'pcid\', u\'sse4_1\', u\'sse4_2\', u\'x2apic\', u\'movbe\', u\'popcnt\', u\'tsc_deadline_timer\', u\'aes\', u\'xsave\', u\'avx\', u\'f16c\', u\'rdrand\', u\'hypervisor\', u\'lahf_lm\', u\'abm\', u\'3dnowprefetch\', u\'ida\', u\'arat\', u\'epb\', u\'pln\', u\'pts\', u\'dtherm\', u\'hwp\', u\'hwp_noitfy\', u\'hwp_act_window\', u\'hwp_epp\', u\'fsgsbase\', u\'tsc_adjust\', u\'bmi1\', u\'hle\', u\'avx2\', u\'smep\', u\'bmi2\', u\'invpcid\', u\'rtm\', u\'rdseed\', u\'adx\', u\'smap\', u\'xsaveopt\'], u\'localhost\': u\'PyDev\', u\'lsb_distrib_id\': u\'CentOS Linux\', u\'username\': u\'root\', u\'fqdn_ip4\': [u\'127.0.0.1\'], u\'saltpath\': u\'/usr/lib/python2.7/site-packages/salt\', u\'fqdn_ip6\': [u\'::1\'], u\'nodename\': u\'PyDev\', u\'saltversion\': u\'2016.11.6\', u\'pythonpath\': [u\'/usr/bin\', u\'/usr/lib64/python27.zip\', u\'/usr/lib64/python2.7\', u\'/usr/lib64/python2.7/plat-linux2\', u\'/usr/lib64/python2.7/lib-tk\', u\'/usr/lib64/python2.7/lib-old\', u\'/usr/lib64/python2.7/lib-dynload\', u\'/usr/lib64/python2.7/site-packages\', u\'/usr/lib64/python2.7/site-packages/gtk-2.0\', u\'/usr/lib/python2.7/site-packages\'], u\'server_id\': 1871050395, u\'biosreleasedate\': u\'07/02/2015\', u\'host\': u\'localhost\', u\'os_family\': u\'RedHat\', u\'oscodename\': u\'CentOS Linux 7 (Core)\', u\'osfinger\': u\'CentOS Linux-7\', u\'pythonversion\': [2, 7, 5, u\'final\', 0], u\'manufacturer\': u\'VMware, Inc.\', u\'num_gpus\': 1, u\'virtual\': u\'VMware\', u\'disks\': [u\'sda\', u\'sr0\', u\'dm-0\', u\'dm-1\', u\'dm-2\'], u\'cpu_model\': u\'Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz\', u\'fqdn\': u\'localhost.localdomain\', u\'pythonexecutable\': u\'/usr/bin/python\', u\'productname\': u\'VMware Virtual Platform\', u\'osarch\': u\'x86_64\', u\'cpuarch\': u\'x86_64\', u\'lsb_distrib_codename\': u\'CentOS Linux 7 (Core)\', u\'osrelease_info\': [7, 2, 1511], u\'locale_info\': {u\'detectedencoding\': u\'UTF-8\', u\'defaultlanguage\': u\'en_US\', u\'defaultencoding\': u\'UTF-8\'}, u\'gpus\': [{u\'model\': u\'SVGA II Adapter\', u\'vendor\': u\'unknown\'}], u\'path\': u\'/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/bin:/sbin\', u\'machine_id\': u\'25770a244db14dacb44ca4f769e0724d\', u\'os\': u\'CentOS\'}'),(4,'PyDev2','{u\'biosversion\': u\'6.00\', u\'kernel\': u\'Linux\', u\'domain\': u\'localdomain\', u\'uid\': 0, u\'zmqversion\': u\'4.1.4\', u\'kernelrelease\': u\'3.10.0-327.el7.x86_64\', u\'selinux\': {u\'enforced\': u\'Enforcing\', u\'enabled\': True}, u\'serialnumber\': u\'VMware-56 4d 66 7f 25 d7 57 84-7b 01 e2 0b 88 9b a5 a7\', u\'pid\': 97964, u\'ip_interfaces\': {u\'lo\': [u\'127.0.0.1\', u\'::1\'], u\'eno16777736\': [u\'192.168.20.22\', u\'fe80::20c:29ff:fe9b:a5a7\'], u\'virbr0-nic\': [], u\'virbr0\': [u\'192.168.122.1\']}, u\'groupname\': u\'root\', u\'shell\': u\'/bin/sh\', u\'mem_total\': 3937, u\'saltversioninfo\': [2016, 11, 6, 0], u\'osmajorrelease\': u\'7\', u\'SSDs\': [], u\'mdadm\': [], u\'id\': u\'PyDev2\', u\'osrelease\': u\'7.2.1511\', u\'ps\': u\'ps -efH\', u\'systemd\': {u\'version\': u\'219\', u\'features\': u\'+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ -LZ4 -SECCOMP +BLKID +ELFUTILS +KMOD +IDN\'}, u\'uuid\': u\'7f664d56-d7225-8457-7b01-e20b889ba5a7\', u\'ip6_interfaces\': {u\'lo\': [u\'::1\'], u\'eno16777736\': [u\'fe80::20c:29ff:fe9b:a5a7\'], u\'virbr0-nic\': [], u\'virbr0\': []}, u\'num_cpus\': 4, u\'hwaddr_interfaces\': {u\'lo\': u\'00:00:00:00:00:00\', u\'eno16777736\': u\'00:0c:29:9b:a5:a7\', u\'virbr0-nic\': u\'52:54:00:9b:0e:3c\', u\'virbr0\': u\'52:54:00:9b:0e:3c\'}, u\'init\': u\'systemd\', u\'ip4_interfaces\': {u\'lo\': [u\'127.0.0.1\'], u\'eno16777736\': [u\'192.168.20.22\'], u\'virbr0-nic\': [], u\'virbr0\': [u\'192.168.122.1\']}, u\'osfullname\': u\'CentOS Linux\', u\'gid\': 0, u\'master\': u\'127.0.0.1\', u\'ipv4\': [u\'127.0.0.1\', u\'192.168.20.22\', u\'192.168.122.1\'], u\'dns\': {u\'domain\': u\'\', u\'sortlist\': [], u\'nameservers\': [u\'202.96.209.5\', u\'114.114.114.114\'], u\'ip4_nameservers\': [u\'202.96.209.5\', u\'114.114.114.114\'], u\'search\': [], u\'ip6_nameservers\': [], u\'options\': []}, u\'ipv6\': [u\'::1\', u\'fe80::20c:29ff:fe9b:a5a7\'], u\'cpu_flags\': [u\'fpu\', u\'vme\', u\'de\', u\'pse\', u\'tsc\', u\'msr\', u\'pae\', u\'mce\', u\'cx8\', u\'apic\', u\'sep\', u\'mtrr\', u\'pge\', u\'mca\', u\'cmov\', u\'pat\', u\'pse36\', u\'clflush\', u\'dts\', u\'mmx\', u\'fxsr\', u\'sse\', u\'sse2\', u\'ss\', u\'syscall\', u\'nx\', u\'pdpe1gb\', u\'rdtscp\', u\'lm\', u\'constant_tsc\', u\'arch_perfmon\', u\'pebs\', u\'bts\', u\'nopl\', u\'xtopology\', u\'tsc_reliable\', u\'nonstop_tsc\', u\'aperfmperf\', u\'eagerfpu\', u\'pni\', u\'pclmulqdq\', u\'ssse3\', u\'fma\', u\'cx16\', u\'pcid\', u\'sse4_1\', u\'sse4_2\', u\'x2apic\', u\'movbe\', u\'popcnt\', u\'tsc_deadline_timer\', u\'aes\', u\'xsave\', u\'avx\', u\'f16c\', u\'rdrand\', u\'hypervisor\', u\'lahf_lm\', u\'abm\', u\'3dnowprefetch\', u\'ida\', u\'arat\', u\'epb\', u\'pln\', u\'pts\', u\'dtherm\', u\'hwp\', u\'hwp_noitfy\', u\'hwp_act_window\', u\'hwp_epp\', u\'fsgsbase\', u\'tsc_adjust\', u\'bmi1\', u\'hle\', u\'avx2\', u\'smep\', u\'bmi2\', u\'invpcid\', u\'rtm\', u\'rdseed\', u\'adx\', u\'smap\', u\'xsaveopt\'], u\'localhost\': u\'PyDev\', u\'lsb_distrib_id\': u\'CentOS Linux\', u\'username\': u\'root\', u\'fqdn_ip4\': [u\'127.0.0.1\'], u\'saltpath\': u\'/usr/lib/python2.7/site-packages/salt\', u\'fqdn_ip6\': [u\'::1\'], u\'nodename\': u\'PyDev\', u\'saltversion\': u\'2016.11.6\', u\'pythonpath\': [u\'/usr/bin\', u\'/usr/lib64/python27.zip\', u\'/usr/lib64/python2.7\', u\'/usr/lib64/python2.7/plat-linux2\', u\'/usr/lib64/python2.7/lib-tk\', u\'/usr/lib64/python2.7/lib-old\', u\'/usr/lib64/python2.7/lib-dynload\', u\'/usr/lib64/python2.7/site-packages\', u\'/usr/lib64/python2.7/site-packages/gtk-2.0\', u\'/usr/lib/python2.7/site-packages\'], u\'server_id\': 1871050395, u\'biosreleasedate\': u\'07/02/2015\', u\'host\': u\'localhost\', u\'os_family\': u\'RedHat\', u\'oscodename\': u\'CentOS Linux 7 (Core)\', u\'osfinger\': u\'CentOS Linux-7\', u\'pythonversion\': [2, 7, 5, u\'final\', 0], u\'manufacturer\': u\'VMware, Inc.\', u\'num_gpus\': 1, u\'virtual\': u\'VMware\', u\'disks\': [u\'sda\', u\'sr0\', u\'dm-0\', u\'dm-1\', u\'dm-2\'], u\'cpu_model\': u\'Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz\', u\'fqdn\': u\'localhost.localdomain\', u\'pythonexecutable\': u\'/usr/bin/python\', u\'productname\': u\'VMware Virtual Platform\', u\'osarch\': u\'x86_64\', u\'cpuarch\': u\'x86_64\', u\'lsb_distrib_codename\': u\'CentOS Linux 7 (Core)\', u\'osrelease_info\': [7, 2, 1511], u\'locale_info\': {u\'detectedencoding\': u\'UTF-8\', u\'defaultlanguage\': u\'en_US\', u\'defaultencoding\': u\'UTF-8\'}, u\'gpus\': [{u\'model\': u\'SVGA II Adapter\', u\'vendor\': u\'unknown\'}], u\'path\': u\'/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/bin:/sbin\', u\'machine_id\': u\'25770a244db14dacb44ca4f769e0724d\', u\'os\': u\'CentOS\'}'),(6,'PyDev','{u\'biosversion\': u\'6.00\', u\'kernel\': u\'Linux\', u\'domain\': u\'localdomain\', u\'uid\': 0, u\'zmqversion\': u\'4.1.4\', u\'kernelrelease\': u\'3.10.0-327.el7.x86_64\', u\'selinux\': {u\'enforced\': u\'Enforcing\', u\'enabled\': True}, u\'serialnumber\': u\'VMware-56 4d 66 7f 25 d7 57 84-7b 01 e2 0b 88 9b a5 a7\', u\'pid\': 18834, u\'ip_interfaces\': {u\'lo\': [u\'127.0.0.1\', u\'::1\'], u\'eno16777736\': [u\'192.168.20.22\', u\'fe80::20c:29ff:fe9b:a5a7\'], u\'virbr0-nic\': [], u\'virbr0\': [u\'192.168.122.1\']}, u\'groupname\': u\'root\', u\'shell\': u\'/bin/sh\', u\'mem_total\': 3937, u\'saltversioninfo\': [2016, 11, 6, 0], u\'osmajorrelease\': u\'7\', u\'SSDs\': [], u\'mdadm\': [], u\'id\': u\'PyDev\', u\'osrelease\': u\'7.2.1511\', u\'ps\': u\'ps -efH\', u\'systemd\': {u\'version\': u\'219\', u\'features\': u\'+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ -LZ4 -SECCOMP +BLKID +ELFUTILS +KMOD +IDN\'}, u\'uuid\': u\'7f664d56-d725-8457-7b01-e20b889ba5a7\', u\'ip6_interfaces\': {u\'lo\': [u\'::1\'], u\'eno16777736\': [u\'fe80::20c:29ff:fe9b:a5a7\'], u\'virbr0-nic\': [], u\'virbr0\': []}, u\'num_cpus\': 4, u\'hwaddr_interfaces\': {u\'lo\': u\'00:00:00:00:00:00\', u\'eno16777736\': u\'00:0c:29:9b:a5:a7\', u\'virbr0-nic\': u\'52:54:00:9b:0e:3c\', u\'virbr0\': u\'52:54:00:9b:0e:3c\'}, u\'init\': u\'systemd\', u\'ip4_interfaces\': {u\'lo\': [u\'127.0.0.1\'], u\'eno16777736\': [u\'192.168.20.22\'], u\'virbr0-nic\': [], u\'virbr0\': [u\'192.168.122.1\']}, u\'osfullname\': u\'CentOS Linux\', u\'gid\': 0, u\'master\': u\'127.0.0.1\', u\'ipv4\': [u\'127.0.0.1\', u\'192.168.20.22\', u\'192.168.122.1\'], u\'dns\': {u\'domain\': u\'\', u\'sortlist\': [], u\'nameservers\': [u\'202.96.209.5\', u\'114.114.114.114\'], u\'ip4_nameservers\': [u\'202.96.209.5\', u\'114.114.114.114\'], u\'search\': [], u\'ip6_nameservers\': [], u\'options\': []}, u\'ipv6\': [u\'::1\', u\'fe80::20c:29ff:fe9b:a5a7\'], u\'cpu_flags\': [u\'fpu\', u\'vme\', u\'de\', u\'pse\', u\'tsc\', u\'msr\', u\'pae\', u\'mce\', u\'cx8\', u\'apic\', u\'sep\', u\'mtrr\', u\'pge\', u\'mca\', u\'cmov\', u\'pat\', u\'pse36\', u\'clflush\', u\'dts\', u\'mmx\', u\'fxsr\', u\'sse\', u\'sse2\', u\'ss\', u\'syscall\', u\'nx\', u\'pdpe1gb\', u\'rdtscp\', u\'lm\', u\'constant_tsc\', u\'arch_perfmon\', u\'pebs\', u\'bts\', u\'nopl\', u\'xtopology\', u\'tsc_reliable\', u\'nonstop_tsc\', u\'aperfmperf\', u\'eagerfpu\', u\'pni\', u\'pclmulqdq\', u\'ssse3\', u\'fma\', u\'cx16\', u\'pcid\', u\'sse4_1\', u\'sse4_2\', u\'x2apic\', u\'movbe\', u\'popcnt\', u\'tsc_deadline_timer\', u\'aes\', u\'xsave\', u\'avx\', u\'f16c\', u\'rdrand\', u\'hypervisor\', u\'lahf_lm\', u\'abm\', u\'3dnowprefetch\', u\'ida\', u\'arat\', u\'epb\', u\'pln\', u\'pts\', u\'dtherm\', u\'hwp\', u\'hwp_noitfy\', u\'hwp_act_window\', u\'hwp_epp\', u\'fsgsbase\', u\'tsc_adjust\', u\'bmi1\', u\'hle\', u\'avx2\', u\'smep\', u\'bmi2\', u\'invpcid\', u\'rtm\', u\'rdseed\', u\'adx\', u\'smap\', u\'xsaveopt\'], u\'localhost\': u\'PyDev\', u\'lsb_distrib_id\': u\'CentOS Linux\', u\'username\': u\'root\', u\'fqdn_ip4\': [u\'127.0.0.1\'], u\'saltpath\': u\'/usr/lib/python2.7/site-packages/salt\', u\'fqdn_ip6\': [u\'::1\'], u\'nodename\': u\'PyDev\', u\'saltversion\': u\'2016.11.6\', u\'pythonpath\': [u\'/usr/bin\', u\'/usr/lib64/python27.zip\', u\'/usr/lib64/python2.7\', u\'/usr/lib64/python2.7/plat-linux2\', u\'/usr/lib64/python2.7/lib-tk\', u\'/usr/lib64/python2.7/lib-old\', u\'/usr/lib64/python2.7/lib-dynload\', u\'/usr/lib64/python2.7/site-packages\', u\'/usr/lib64/python2.7/site-packages/gtk-2.0\', u\'/usr/lib/python2.7/site-packages\'], u\'server_id\': 1871050395, u\'biosreleasedate\': u\'07/02/2015\', u\'host\': u\'localhost\', u\'os_family\': u\'RedHat\', u\'oscodename\': u\'CentOS Linux 7 (Core)\', u\'osfinger\': u\'CentOS Linux-7\', u\'pythonversion\': [2, 7, 5, u\'final\', 0], u\'manufacturer\': u\'VMware, Inc.\', u\'num_gpus\': 1, u\'virtual\': u\'VMware\', u\'disks\': [u\'sda\', u\'sr0\', u\'dm-0\', u\'dm-1\', u\'dm-2\'], u\'cpu_model\': u\'Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz\', u\'fqdn\': u\'localhost.localdomain\', u\'pythonexecutable\': u\'/usr/bin/python\', u\'productname\': u\'VMware Virtual Platform\', u\'osarch\': u\'x86_64\', u\'cpuarch\': u\'x86_64\', u\'lsb_distrib_codename\': u\'CentOS Linux 7 (Core)\', u\'osrelease_info\': [7, 2, 1511], u\'locale_info\': {u\'detectedencoding\': u\'UTF-8\', u\'defaultlanguage\': u\'en_US\', u\'defaultencoding\': u\'UTF-8\'}, u\'gpus\': [{u\'model\': u\'SVGA II Adapter\', u\'vendor\': u\'unknown\'}], u\'path\': u\'/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/bin:/sbin\', u\'machine_id\': u\'25770a244db14dacb44ca4f769e0724d\', u\'os\': u\'CentOS\'}');
/*!40000 ALTER TABLE `salt_grains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salt_returns`
--

DROP TABLE IF EXISTS `salt_returns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salt_returns` (
  `alter_time` datetime NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fun` varchar(50) NOT NULL,
  `jid` varchar(255) NOT NULL,
  `returns` longtext NOT NULL,
  `minion_id` varchar(255) NOT NULL,
  `success` varchar(10) NOT NULL,
  `full_ret` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salt_returns`
--

LOCK TABLES `salt_returns` WRITE;
/*!40000 ALTER TABLE `salt_returns` DISABLE KEYS */;
/*!40000 ALTER TABLE `salt_returns` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-22 16:21:12
