from django.conf.urls import  url
import views,envviews



urlpatterns = [
    url(r'^host_list/$', views.List_Host, name='List_Host'),
    url(r'show_host_in_table',views.show_host_in_table,name='show_host_in_table'),
    url(r'^create_host/$', views.cmdb_host_create, name='create_host'),
    url(r'^edit_host/(?P<pk>\d+)/$', views.cmdb_host_update, name='edit_host'),
    url(r'^delete_host/(?P<pk>\d+)/$', views.cmdb_host_delete, name='delete_host'),
    url(r'sync',views.sync_asset_to_host,name='sync_asset_to_host'),
    url(r'^env_add/$', envviews.IpSourceView.as_view(),name='env_add'),
    ]