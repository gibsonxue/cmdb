from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from website.views import Home,About

#from django.contrib import admin
#admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^$', 'website.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$',include('dashboard.urls')),
    #url(r'^dashboard/',include('dashboard.urls')),
    url(r'^about/$',About),
    url(r'^accounts/',include('UserManage.urls')),
    url(r'^cmdb/', include('cmdb.urls')),
    url(r'^minions/', include('minions.urls')),
    url(r'^publisher/', include('publisher.urls')),
    #url(r'^execute/', include('execute.urls'))
    #static
] + static(settings.STATIC_URL, document_root = settings.STATICFILES_DIRS)