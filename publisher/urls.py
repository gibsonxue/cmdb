from django.conf.urls import url
import views

urlpatterns = [
        url(r'project_view',views.ProjectListView.as_view(),name='project_view'),
        url(r'project_view/edit/(?P<id>\d+)]',views.ProjectListView.as_view(),name='project_edit'),
        url(r'project_view/detail/(?P<id>\d+)]', views.ProjectDetailView.as_view(), name='project_detail'),

]